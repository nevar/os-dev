# Copyright © 2013 Rafal Jastrzebski <mail@nevar.pl>
# All rights reserved.

cmake_minimum_required(VERSION 3.2)
project(NevarOS CXX ASM)

set(ARCH_DIR ${CMAKE_SOURCE_DIR}/arch/${ARCH})
include(${ARCH_DIR}/toolchain.cmake)

## uncomment for debug
#set(CMAKE_VERBOSE_MAKEFILE true)
#set(DEBUG_LOG true)

execute_process(COMMAND ${CMAKE_CXX_COMPILER} -print-file-name=crtbegin.o OUTPUT_VARIABLE CRTBEGIN_OBJ OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND ${CMAKE_CXX_COMPILER} -print-file-name=crtend.o OUTPUT_VARIABLE CRTEND_OBJ OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND ${CMAKE_CXX_COMPILER} -print-file-name=crti.o OUTPUT_VARIABLE CRTI_OBJ OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND ${CMAKE_CXX_COMPILER} -print-file-name=crtn.o OUTPUT_VARIABLE CRTN_OBJ OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND ${CMAKE_CXX_COMPILER} -print-libgcc-file-name OUTPUT_VARIABLE LIBGCC OUTPUT_STRIP_TRAILING_WHITESPACE)

# Debug
message("crti:     ${CRTI_OBJ}")
message("crtn:     ${CRTN_OBJ}")
message("crtbegin: ${CRTBEGIN_OBJ}")
message("crtend:   ${CRTEND_OBJ}")
message("libgcc:   ${LIBGCC}")

# -fno-rtti			no dynamic casting but object sizes are few bytes smaller
# -fno-exceptions	no exceptions
# -ffreestanding	compilation without standard library
# -nostdlib			no references to the standard library

# warning flags
set(CMAKE_WARNING_FLAGS_COMMON "-Wall -Weffc++ -Wold-style-cast -Woverloaded-virtual -Wswitch -Wunused -Wuninitialized -Wfloat-equal -Wshadow")
set(CMAKE_WARNING_FLAGS "${CMAKE_WARNING_FLAGS_COMMON} ${CMAKE_WARNING_FLAGS_ARCH}")

set(CMAKE_BASE_FLAGS "-std=c++11 -fno-rtti -fno-exceptions -ffreestanding -nostdlib -fno-use-cxa-atexit")
set(CMAKE_PREINCLUDES "-include Base/stdc++.h")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g3 -o0 ${CMAKE_BASE_FLAGS} ${CMAKE_WARNING_FLAGS} ${CMAKE_PREINCLUDES}")
set(CMAKE_CXX_LINK_FLAGS "-Wl,-nostdlib,-znodefaultlib,-zmax-page-size=0x1000")


#set(CMAKE_FLAGS "-nostdlib -g -fno-rtti -fno-exceptions -ffreestanding -nostdinc++")
#set(CMAKE_CAMPILE_FLAGS "-std=c++11 -Wall -Weffc++ -Wold-style-cast -Woverloaded-virtual -Wswitch -Wunused -Wuninitialized -Wmaybe-uninitialized -Wsuggest-final-methods -Wsuggest-final-types -Wsuggest-override -Wduplicated-cond -Wfloat-equal -Wshadow")
#set(CMAKE_CAMPILE_FLAGS "-Wall -Weffc++ -Wold-style-cast -Woverloaded-virtual -Wswitch -Wunused -Wuninitialized -Wmaybe-uninitialized -Wsuggest-final-methods -Wsuggest-final-types -Wsuggest-override -Wfloat-equal -Wshadow")
#set(CMAKE_FLAGS "-g3 -o0 ${CMAKE_CAMPILE_FLAGS} -fno-rtti -fno-exceptions -ffreestanding -nostdlib -fno-use-cxa-atexit")
#set(CMAKE_PREINCLUDES "-include global.h -include stdint.h")
#set(CMAKE_CXX_FLAGS "${CMAKE_FLAGS} ${CMAKE_PREINCLUDES}")

# check all following option if they r realy needed
#set(CMAKE_LD_FLAGS "--nostartfiles")
#set(CMAKE_LD_FLAGS "-Wl,-nostdlib,-nodefaultlib")
#set(CMAKE_LD_FLAGS "-nostdlib -znodefaultlib -zmax-page-size=0x1000")

set(CMAKE_EXE_LINKER_FLAGS "-T ${ARCH_DIR}/linker.ld ${CMAKE_LD_FLAGS}")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> ${CRTI_O} ${CRTBEGIN_O} <OBJECTS> ${LIBGCC} ${CRTEND_O} ${CRTN_O} -o <TARGET> <LINK_LIBRARIES>")

message("Tools for building:")
message("c++ compiler: ${CMAKE_CXX_COMPILER}")
message("asm compiler: ${CMAKE_ASM_COMPILER}")
message("linker:       ${CMAKE_LINK_EXECUTABLE}")
message("objdump:      ${CMAKE_OBJCOPY}")

file(GLOB_RECURSE G_SOURCES_ARCH ${ARCH_DIR}/*.cpp)
file(GLOB_RECURSE G_SOURCES_BASE
	Base/*.cpp
	Memory/*.cpp
	Debug/*.cpp
	Data/*.cpp)

set(G_SOURCES
	${G_SOURCES_BASE}
	${G_SOURCES_ARCH}
	kernel.cpp
	boot.o
	)

execute_process(COMMAND ${CMAKE_C_COMPILER} -print-file-name=crti.o OUTPUT_VARIABLE CRTI_OBJ OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND ${CMAKE_C_COMPILER} -print-file-name=crtn.o OUTPUT_VARIABLE CRTN_OBJ OUTPUT_STRIP_TRAILING_WHITESPACE)

## Prepare object files order for linker
list(LENGTH G_OBJECTS count)
math(EXPR max_index ${count}-1 )
foreach(i RANGE ${max_index} 0)
	list(GET G_OBJECTS ${i} x)
	string(REGEX MATCH "crti.o$" result ${x})
	if( "${result}" STREQUAL "crti.o" )
		set(CRTI_OBJ ${x})
		list(REMOVE_AT G_OBJECTS ${i})
	endif()

	string(REGEX MATCH "crtn.o$" result ${x})
	if( "${result}" STREQUAL "crtn.o" )
		set(CRTN_OBJ ${x})
		list(REMOVE_AT G_OBJECTS ${i})
	endif()
endforeach()

message("objects: ${G_OBJECTS}")

## This is for debug only
if( DEFINED DEBUG_LOG )
	message("x:       ${G_FOREIGN_SOURCES}")
	message("sources: ${G_SOURCES}")
	message("headers: ${G_HEADERS}")
	message("others:  ${G_OTHERS}")
	message("objects: ${G_OBJECTS}")
endif()

include_directories(${CMAKE_SOURCE_DIR})
include_directories(${ARCH_DIR})

add_executable(kernel.elf ${G_SOURCES})
