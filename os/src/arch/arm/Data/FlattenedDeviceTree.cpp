/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "FlattenedDeviceTree.h"

FlattenedDeviceTree::FlattenedDeviceTree(FDTHeader *pFdt)
	: m_pHeader(NULL)
{
	if (pFdt && invertBytes(pFdt->magic) == FDTHeaderMagic)
	{
		m_pHeader = pFdt;
	}
}

FlattenedDeviceTree::FlattenedDeviceTree(size_t pFdt)
	: FlattenedDeviceTree(reinterpret_cast<FDTHeader*>(pFdt))
{
}

void FlattenedDeviceTree::dumpHeader()
{
	DBG << "FDT:                     " << dhex(m_pHeader);
	DBG << "  magic:                   " << dhex(invertBytes(m_pHeader->magic));
	DBG << "  total size:              " << dhex(invertBytes(m_pHeader->totalSize));
	DBG << "  structure offset:        " << dhex(invertBytes(m_pHeader->structureOffset));
	DBG << "  strings offset:          " << dhex(invertBytes(m_pHeader->stringsOffset));
	DBG << "  memory reserve map:      " << dhex(invertBytes(m_pHeader->memoryReserveMapOffset));
	DBG << "  version:                 " << dhex(invertBytes(m_pHeader->version));
	DBG << "  last compatibile version:" << dhex(invertBytes(m_pHeader->lastCompatibileVersion));

	if (m_pHeader->version >= 2)
		DBG << "  boot CPU id:             " << dhex(invertBytes(m_pHeader->bootCPUId));

	if (m_pHeader->version >= 3)
		DBG << "  strings block size:      " << dhex(invertBytes(m_pHeader->stringsBlockSize));

	if (m_pHeader->version >= 17)
		DBG << "  structure block size:    " << dhex(invertBytes(m_pHeader->structureBlockSize));
}

void FlattenedDeviceTree::dumpReservedMemory()
{
	if (!m_pHeader)
		return;

	const FDTReserveMemory* pReservedMemoryItem = m_pHeader->getReserveMemoryAddress();

	DBG << "Reserved memory" << dhex(pReservedMemoryItem);
	size_t count = 0;

	while(true)
	{
		if (pReservedMemoryItem && !pReservedMemoryItem->address)
			break;

		DBG << "  address:" << dhex(invertBytes(pReservedMemoryItem->address));
		DBG << "  size:   " << dhex(invertBytes(pReservedMemoryItem->size));
		pReservedMemoryItem++;
		++count;
	}

	if (!count)
		DBG << "  empty";
}

const char* FlattenedDeviceTree::getString(size_t offset) const
{
	if (!m_pHeader)
		return NULL;

	return &m_pHeader->getStringsAddress()[offset];
}

void FlattenedDeviceTree::dumpStructure() const
{
	FlattenedDeviceTree::Iterator i(*this);
	const char* path[10];
	size_t index = 0;

	while (!i.isEnd())
	{
		if (strlen(i.node().name()) > 0)
		{
			path[index++] = "/";
			path[index++] = i.node().name();
		}

		if (i.node().type() == FDTNode::E_PROP ||
			i.node().type() == FDTNode::E_BEGIN_NODE)
		{
			for (size_t x = 0; x < index; x++)
			{
				DBGd(Debug::DEBUG_FLAGS_NONE) << path[x];
			}

			if (index > 0)
			{
				DBGd(Debug::DEBUG_FLAGS_CRLF_AT_END)
						<< " #" << i.node().dataLength() << " "
						<< dbuf<uint8_t>(i.node().data(), i.node().dataLength());
			}
		}

		if (i.node().type() == FDTNode::E_END_NODE ||
			i.node().type() == FDTNode::E_PROP)
		{
			index -= 2;
		}

		++i;
	}
}

bool FlattenedDeviceTree::Iterator::isEnd() const
{
	return m_node.type() == FDTNode::E_FDT_END;
}
FlattenedDeviceTree::Iterator& FlattenedDeviceTree::Iterator::operator++()
{
	m_node.next();
	return *this;
}

FlattenedDeviceTree::Iterator::Iterator(const FlattenedDeviceTree &fdt)
	: m_node(fdt)
	, m_fdt(fdt)
{
}

const FDTNode &FlattenedDeviceTree::Iterator::node() const
{
	return m_node;
}
