/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef FDTNODE_H
#define FDTNODE_H

#include "global.h"

struct FDTReserveMemory
{
	uint64_t address;
	uint64_t size;
};

struct FDTHeader
{
	uint32_t magic;
	uint32_t totalSize;
	uint32_t structureOffset;
	uint32_t stringsOffset;
	uint32_t memoryReserveMapOffset;
	uint32_t version;
	uint32_t lastCompatibileVersion;

	// version 2
	uint32_t bootCPUId;

	// version 3
	uint32_t stringsBlockSize;

	// version 17
	uint32_t structureBlockSize;

	const FDTReserveMemory* getReserveMemoryAddress() const
	{
		const uint8_t* pByte = reinterpret_cast<const uint8_t*>(this);
		return reinterpret_cast<const FDTReserveMemory*>(&pByte[invertBytes(this->memoryReserveMapOffset)]);
	}

	const uint8_t* getStructureAddress() const
	{
		const uint8_t* pByte = reinterpret_cast<const uint8_t*>(this);
		return &pByte[invertBytes(this->structureOffset)];
	}

	const char* getStringsAddress() const
	{
		const char* pByte = reinterpret_cast<const char*>(this);
		return &pByte[invertBytes(this->stringsOffset)];
	}
};

class FlattenedDeviceTree;

class FDTNode
{
public:
	enum NodeType
	{
		E_BEGIN_NODE = 1,
		E_END_NODE = 2,
		E_PROP = 3,
		E_NOP = 4,
		E_FDT_END = 9
	};

	FDTNode(const FlattenedDeviceTree& pFdt);

	NodeType type() const;
	const char* name() const;
	const uint8_t* data() const;
	size_t dataLength() const;
	void next();

private:
	const FlattenedDeviceTree& m_fdt;
	const uint8_t* m_data;
};

#endif // FDTNODE_H
