set(PLATFORM beaglexm)

set(CMAKE_FIND_ROOT_PATH  /usr/local/arm-none-eabi)
set(CMAKE_CXX_COMPILER    /usr/local/bin/arm-none-eabi-c++)
set(CMAKE_ASM_COMPILER    /usr/local/bin/arm-none-eabi-as)
set(CMAKE_CXX_FLAGS "--sysroot=/usr/local/bin/arm-none-eabi -march=armv7-a")

set(CMAKE_WARNING_FLAGS_ARCH "-Wsuggest-override")

set(UKERNEL_START_ADDRESS 0x82000000)               # if this is changed then change also the value from linker script
set(UKERNEL_EXEC_ADDRESS ${UKERNEL_START_ADDRESS})
set(IMG_DIR ${CMAKE_BINARY_DIR}/image)

add_custom_command(
	OUTPUT boot.o
	COMMAND ${CMAKE_ASM_COMPILER} ${CMAKE_SOURCE_DIR}/arch/arm/boot.s -o ${CMAKE_BINARY_DIR}/boot.o
	DEPENDS ${CMAKE_SOURCE_DIR}/arch/arm/boot.s ${CMAKE_ASM_COMPILER}
	COMMENT "Building object ${CMAKE_BINARY_DIR}/boot.o"
	)

# create kernel.bin
add_custom_command(
	OUTPUT ${CMAKE_BINARY_DIR}/kernel.bin
	COMMAND ${CMAKE_OBJCOPY} -O binary ${CMAKE_BINARY_DIR}/kernel.elf ${CMAKE_BINARY_DIR}/kernel.bin
	DEPENDS ${CMAKE_BINARY_DIR}/kernel.elf
	COMMENT "[Creating] kernel.bin"
	)

# crete uKernel u-boot kernel image file
add_custom_command(
	OUTPUT ${CMAKE_BINARY_DIR}/uKernel
	COMMAND mkimage -A arm -O linux -T kernel -C none -a ${UKERNEL_START_ADDRESS} -e ${UKERNEL_EXEC_ADDRESS} -n ${CMAKE_PROJECT_NAME} -d ${CMAKE_BINARY_DIR}/kernel.bin ${CMAKE_BINARY_DIR}/uKernel > /dev/null
	DEPENDS ${CMAKE_BINARY_DIR}/kernel.bin
	COMMENT "[Creating] uKernel"
	)

# create sd card image file
add_custom_command(
	OUTPUT cmd_create.sd.image
	COMMAND mkdir -p ${IMG_DIR}/sd
	COMMAND test -e ${IMG_DIR}/sd/sd.img || mkdiskimage -F -M ${IMG_DIR}/sd/sd.img 512 255 63
	COMMENT "[Generate] sd.img"
	)

# create sd card platform image file
add_custom_command(
	OUTPUT cmd_create.platform.image.sd
	COMMAND mkdir -p ${IMG_DIR}/sd/${PLATFORM}
	COMMAND test -e ${IMG_DIR}/sd/${PLATFORM}/sd.img || cp ${IMG_DIR}/sd/sd.img ${IMG_DIR}/sd/${PLATFORM}/sd.img
	DEPENDS cmd_create.sd.image
	COMMENT "[Generate] ${PLATFORM} sd.img"
	)

# create sd boot partititon tree
add_custom_command(
	OUTPUT cmd_create.sd.image.boot
	COMMAND rm -rf ${IMG_DIR}/sd/${PLATFORM}/boot
	COMMAND mkdir -p ${IMG_DIR}/sd/${PLATFORM}/boot
	COMMAND cp ${CMAKE_BINARY_DIR}/uKernel ${IMG_DIR}/sd/${PLATFORM}/boot
	COMMAND cp ${ARCH_DIR}/config/preEnv.txt ${IMG_DIR}/sd/${PLATFORM}/boot/
	COMMAND cp ${ARCH_DIR}/config/omap3-beagle-xm.dtb ${IMG_DIR}/sd/${PLATFORM}/boot/
	COMMAND cp ${ARCH_DIR}/config/MLO ${IMG_DIR}/sd/${PLATFORM}/boot/
	COMMAND cp ${ARCH_DIR}/config/u-boot.bin ${IMG_DIR}/sd/${PLATFORM}/boot/
	DEPENDS ${CMAKE_BINARY_DIR}/uKernel ${ARCH_DIR}/config/preEnv.txt ${ARCH_DIR}/config/omap3-beagle-xm.dtb ${ARCH_DIR}/config/MLO ${ARCH_DIR}/config/u-boot.bin
	COMMENT "[Creating] sd boot parition tree. ${OS_DIR}/init.env $ENV{OS_INSTALL_DIR}"
	)

# create compleate sd image file
add_custom_command(
	OUTPUT cmd_image.sd
	COMMAND mcopy -o -i ${IMG_DIR}/sd/${PLATFORM}/sd.img@@32256 ${IMG_DIR}/sd/${PLATFORM}/boot/* ::
	DEPENDS cmd_create.platform.image.sd cmd_create.sd.image.boot
	COMMENT "[Creating] sd image: ${OS_DIR}/os/image/sd/${PLATFORM}/sd.img"
	)
add_custom_target(img DEPENDS cmd_image.sd)

# run arm qemu
add_custom_command(
	OUTPUT cmd_qemu.arm.run
	COMMAND /tmp/qemu/bin/qemu-system-arm -M ${PLATFORM} -drive if=sd,cache=writeback,file=${IMG_DIR}/sd/${PLATFORM}/sd.img -clock unix -device usb-kbd -device usb-mouse -nographic
	DEPENDS cmd_image.sd
	COMMENT "Runing qemu ${QEMU} -M ${PLATFORM} -drive if=sd,cache=writeback,file=${OS_DIR}/os/image/sd/${PLATFORM}/sd.img -clock unix -device usb-kbd -device usb-mouse -nographic"
	)
add_custom_target(run DEPENDS cmd_qemu.arm.run)
