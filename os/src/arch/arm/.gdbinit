set architecture i386:x86-64:intel
set disassembly-flavor intel
set print asm-demangle on

file kernel.elf
b kernel_main
target remote tcp::1234

