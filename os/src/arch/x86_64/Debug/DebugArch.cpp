/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "DebugArch.h"

// this is operator needed for cxx 14 check what to do with it
void operator delete(void *, unsigned long) throw()
{
	char *p = 0;
	p[-1] = -1;
}

// Screen definition
const int stdout_charsHeight = 25;
const int stdout_charsWidth = 80;
uint8_t* const stdout_baseAddress = reinterpret_cast<uint8_t*>(0xB8000);

#define UART0_BASE 0x03F8   // COM1
#define UART1_BASE 0x02F8   // COM2
#define UART2_BASE 0x03E8   // ...
#define UART3_BASE 0x02E8

#define UART_DATA 0

#define Black         0
#define Blue          1
#define Green         2
#define Cyan          3
#define Red           4
#define Magenta       5
#define Brown         6
#define LightGray     7
#define DarkGray      8
#define LightBlue     9
#define LightGreen   10
#define LightCyan    11
#define LightRed     12
#define LightMagenta 13
#define Yellow       14
#define White        15

struct Cursor
{
	Cursor() : x(0), y(0) {}

	int  x;
	int  y;
};

static Cursor cursor;

void DebugArch::printChar(const uint8_t aChar)
{
	uint8_t * volatile pAddr = 0;
	uint16_t port = UART0_BASE + UART_DATA;
	outb(port, aChar);

	// write char to stdout
	if (0x0D == aChar)
	{
	}
	else if (0x0A == aChar)
	{
		cursor.x = stdout_charsWidth;
	}
	else
	{
		pAddr = &stdout_baseAddress[2 * (stdout_charsWidth * cursor.y + cursor.x)];
		*pAddr = aChar;
		pAddr++;
		*pAddr = (Black << 4) + LightGray;
		cursor.x++;
	}

	// change cursor position
	if (cursor.x == stdout_charsWidth)
	{
		cursor.y++;
		cursor.x = 0;

		if (cursor.y == stdout_charsHeight)
		{
			// scroll screen
			const int bytesToMove = stdout_charsWidth * (stdout_charsHeight - 1) * 2;
			pAddr = stdout_baseAddress;
			cursor.y = stdout_charsHeight - 1;

			for(unsigned int i = 0; i < bytesToMove / sizeof(uint64_t); i++)
			{
				uint64_t* volatile pAddr64 = reinterpret_cast<uint64_t*>(pAddr);
				pAddr64[i] = pAddr64[i + stdout_charsWidth * 2 / sizeof(uint64_t)];
			}

			for(int i = bytesToMove; i < stdout_charsWidth * stdout_charsHeight * 2; i += 2)
			{
				pAddr[i] = ' ';
				pAddr[i+1] = (Black << 4) + White;
			}
		}
	}
}
