/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef DEBUGARCH_H
#define DEBUGARCH_H

#include "Debug/DebugA.h"

/**
 * @brief The DebugArch class This is implementation of DebugA interface
 * and it musn't be extended in the future. This type can only be accessible
 * through base class DebugA
 */
class DebugArch final : public DebugA
{
	friend class DebugA;

private:
public: // remove this line FIXME
	DebugArch() {}
	virtual ~DebugArch() {}

	virtual void printChar(const uint8_t aChar) override final;
};

#endif // DEBUGARCH_H