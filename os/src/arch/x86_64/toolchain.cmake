enable_language(ASM_NASM)

add_custom_command(
	OUTPUT boot.o
	COMMAND ${CMAKE_ASM_NASM_COMPILER} -f elf64 ${CMAKE_SOURCE_DIR}/arch/x86_64/boot.asm -o ${CMAKE_BINARY_DIR}/boot.o
	DEPENDS ${CMAKE_SOURCE_DIR}/arch/x86_64/boot.asm ${CMAKE_ASM_NASM_COMPILER}
	COMMENT "Building object ${CMAKE_BINARY_DIR}/boot.o"
	)

# create iso image
add_custom_command(
	OUTPUT cmd_create.iso.image
	COMMAND mkdir -p ${CMAKE_BINARY_DIR}/isodir
	COMMAND mkdir -p ${CMAKE_BINARY_DIR}/isodir/boot
	COMMAND cp ${CMAKE_BINARY_DIR}/kernel.elf ${CMAKE_BINARY_DIR}/isodir/boot/kernel.bin
	COMMAND mkdir -p ${CMAKE_BINARY_DIR}/isodir/boot/grub
	COMMAND cp ${ARCH_DIR}/grub.cfg ${CMAKE_BINARY_DIR}/isodir/boot/grub/grub.cfg
	COMMAND grub-mkrescue -o ${CMAKE_BINARY_DIR}/os.iso ${CMAKE_BINARY_DIR}/isodir > /dev/null 2>&1
	DEPENDS kernel.elf ${ARCH_DIR}/grub.cfg
	COMMENT os.iso
	)
add_custom_target(img DEPENDS cmd_create.iso.image)

add_custom_command(
	OUTPUT cmd_gdbinit
	COMMAND cp ${ARCH_DIR}/.gdbinit ${CMAKE_BINARY_DIR}/.gdbinit
	DEPENDS ${ARCH_DIR}/.gdbinit
	)
add_custom_target(gdbinit ALL DEPENDS cmd_gdbinit)

# run x86 qemu
add_custom_command(
	OUTPUT cmd_qemu.run
	COMMAND qemu-system-x86_64 -cdrom os.iso --nographic
	DEPENDS cmd_create.iso.image
	COMMENT "Runing qemu"
	)
add_custom_target(run DEPENDS cmd_qemu.run)
