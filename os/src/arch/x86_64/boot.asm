; Copyright © 2013 Rafal Jastrzebski <mail@nevar.pl>
; All rights reserved.

%define CS_32 _CS_32 - GDT
%define DS_32 _DS_32 - GDT
%define CS_64 _CS_64 - GDT
%define DS_64 _DS_64 - GDT
%define OSFXSR 0x200

%define MAX_MEMORY  0x1000000

section .multiboot
global multiboot_start
multiboot_start:
align 4
	magic		dd	0x1BADB002
    flags		dd	0x3
    checksum	dd	- ( 0x1BADB002 + 0x3 )
                dd  0, 0, 0, 0, 0, 0, 0, 0, 0
    mis         dq  0

section .bss
stack_bottom:
    resb 0x10000
stack_top:

[BITS 32]
section .text
global boot_start
boot_start:
    cli
    mov	esp,	stack_top
    mov     [mis],  ebx

    lgdt [GDTR]
	jmp  CS_32:.new_GDT
.new_GDT:
	mov	ax,	DS_32
	mov	ds,	ax
	mov	es,	ax
	mov	ss,	ax
	mov	esp,	stack_top

	jmp .skipp

	mov	esi,	0x200000
	mov	edi,	0xB8000
	mov	ah,	0x7
	mov	ecx,	256
.rep:
	mov	al,	[esi]
	shr	al,	4
	cmp	al,	0x9
	jbe	.skip
	add	al,	7
.skip:
	add	al,	0x30
	mov	[edi],	ax
	add	edi,	2

	mov	al,	[esi]
	and	al,	0xF
	cmp	al,	0x9
	jbe	.skip2
	add	al,	7
.skip2:
	add	al,	0x30
	mov	[edi],	ax
	add	edi,	2
	add	edi,	2
	inc	esi
	dec	ecx
	jnz	.rep

	jmp $
.skipp:

        ; pml4 = 0x90000
        ; plm3 = 0x91000
        ; plm2 = 0x92000
        ; plm1 = 0x94000 0 - 2 MB
        ;        0x95000 2 - 4 MB

;        0x96000 -4 - -2MB
;        0x97000 -2 -  0MB

        ;mapping 1:1 memory
        mov [0x90000], dword 0x91003; pml3
        mov [0x90004], dword 0
        mov [0x91000], dword 0x92003; pml2
        mov [0x91004], dword 0
        mov [0x92000], dword 0x94003; pml1
        mov [0x92004], dword 0
        mov [0x92008], dword 0x95003; pml1
        mov [0x9200C], dword 0

        mov [0x92000 + 0x1000 - 0x10 ], dword 0x96003; pml1
        mov [0x92000 + 0x1000 - 0x0C ], dword 0
        mov [0x92000 + 0x1000 - 0x08 ], dword 0x97003; pml1
        mov [0x92000 + 0x1000 - 0x04 ], dword 0

        mov [0x96000 + 0x1000 - 0x08 ], dword 0x92003; pml2
        mov [0x96000 + 0x1000 - 0x04 ], dword 0
        mov [0x97000 + 0x00 ], dword 0x94003; pml1
        mov [0x97000 + 0x04 ], dword 0
        mov [0x97000 + 0x08 ], dword 0x95003; pml1
        mov [0x97000 + 0x0C ], dword 0
        mov [0x97000 + 0x10 ], dword 0x98003; pml1
        mov [0x97000 + 0x14 ], dword 0

        ; memory bit table
        mov [0x92010 ], dword 0x98003; pml1
        mov [0x92014 ], dword 0
        mov [0x98000 ], dword 0x99003; page
        mov [0x98004 ], dword 0

        xor eax, eax
        xor edx, edx
.B2:
        mov ebx, edx
        or  ebx, 3
        mov [0x94000 + eax], ebx
        mov [0x94004 + eax], dword 0
        add edx, 0x1000
        add eax, 8
        cmp eax, 4096*2
        jne .B2

        ;PAE
        mov eax, cr4 
        bts eax, 5
        or  eax,    OSFXSR
        mov cr4, eax

        ;Loading new PML4
        mov eax, 0x90000
        mov cr3, eax

        ;Switching to LM
        mov ecx, 0xC0000080
        rdmsr
        bts eax, 8
        wrmsr

        ;Paging enabling
        mov eax, cr0
        bts eax, 31
        mov cr0, eax

        mov ax, DS_64
        mov ds, ax
        jmp CS_64:code64

GDTR:
        dw GDT_END - GDT - 1
        dd GDT
        dd 0
align 16
GDT:
                dq 0
_CS_32  dq 0x00CF9A000000FFFF
_DS_32  dq 0x00CF92000000FFFF
_CS_64  dq 0x00A09A0000000000
_DS_64  dq 0x00A0920000000000
GDT_END:

[BITS 64]
code64:
extern kernel_main
	call	kernel_main
	cli
	hlt
	jmp $

global acquire_lock
acquire_lock:
    pushf

    lock bts dword [rdi], 0
    jnc .aquired

.retest:
    pause
    test dword [rdi], 1
    jnz .retest

    lock bts dword [rdi], 0
    jc .retest

.aquired:
    popf
    ret

global release_lock
release_lock:
    mov dword [rdi], 0
    ret

global outb
outb:
    push    rax
    push    rdx
    mov rdx,    rdi
    mov rax,    rsi
    out dx, al
    pop     rdx
    pop     rax
    ret

global inb
inb:
    push    rdx
    mov rdx,    rdi
    in  al,     dx
    pop     rdx
    ret

global getcr3
getcr3:
    mov rax,    cr3
    ret

global setcr3
setcr3:
    mov cr3,    rdi
    ret


global cpu_break
cpu_break:
    mov rbx,    rdi
    mov al, [rbx]
    nop
    mov al, al
    nop
    ret

global cr3_reload
cr3_reload:
    mov rax,    cr3
    mov cr3,    rax
    ret
