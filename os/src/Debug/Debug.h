/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef DEBUG_H
#define DEBUG_H

#include "global.h"
#include "Debug/DebugA.h"
#include "Debug/DebugArch.h"

class dhex
{
public:
	dhex(size_t aValue)
		: m_value(aValue)
		, m_size(sizeof(size_t))
	{}

	dhex(void* aValue)
		: m_value(static_cast<size_t>(reinterpret_cast<intptr_t>(aValue)))
		, m_size(sizeof(aValue))
	{}

	dhex(const void* aValue)
		: m_value(static_cast<size_t>(reinterpret_cast<intptr_t>(aValue)))
		, m_size(sizeof(aValue))
	{}

	size_t value() const { return m_value; }
	size_t size() const { return m_size; }

private:
	size_t m_value;
	size_t m_size;
};

template <class T>
class dbuf
{
public:
	dbuf(const uint8_t* pValue, size_t size)
		: m_pValue(pValue)
		, m_size(size)
	{}

	const uint8_t* m_pValue;
	size_t m_size;
};

class Debug
{
public:
	enum Flags
	{
		DEBUG_FLAGS_NONE = 0,
		DEBUG_FLAGS_CRLF_AT_END = 1,
		DEBUG_FLAGS_SPACE_AFTER_EACH = 2,
		DEBUG_FLAGS_CRLF_AFTER_EACH = 4,
		DEBUG_FLAGS_PRINT_PREFIX = 8,
		DEBUG_FLAGS_DEFAULT = (DEBUG_FLAGS_CRLF_AT_END | DEBUG_FLAGS_SPACE_AFTER_EACH | DEBUG_FLAGS_PRINT_PREFIX)
	};

	Debug(Debug::Flags flags = DEBUG_FLAGS_DEFAULT);
	~Debug();

	const Debug& operator<<(const char* aString) const;
	const Debug& operator<<(const size_t value) const;
	const Debug& operator<<(const dhex hexValue) const;

	template <class T>
	const Debug& operator<<(const dbuf<T> buffer) const
	{
		printBuffer(buffer.m_pValue, buffer.m_size);
		return *this;
	}

private:
	void printBuffer(const uint8_t* pBuffer, size_t count) const;
	void processPostFlags() const;
	void print(size_t value, bool leadingZeros) const;
	void printHex(size_t value, size_t size, bool leadingZeros) const;

private:
	Flags m_flags;
};

#define ERR Debug() << "[ERR]" << __FILE__ << __LINE__ << ":"
#define INF Debug() << "[INF]" << __FILE__ << __LINE__ << ":"
#define DBG Debug() << "[DBG]" << __FILE__ << __LINE__ << ":"
#define DBGd(x) Debug(x)

#endif // DEBUG_H
