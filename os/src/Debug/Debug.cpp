/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "Debug.h"
#include "DebugA.h"

static uint8_t hexToASCII(uint8_t c)
{
	if (c > 9) {
		c += 7;
	}
	return c+0x30;
}

Debug::Debug(Flags flags)
	: m_flags(flags)
{
}

Debug::~Debug()
{
	if ( !(m_flags & DEBUG_FLAGS_CRLF_AFTER_EACH) &&
		m_flags & DEBUG_FLAGS_CRLF_AT_END)
	{
		DebugA::archImp()->printChar('\n');
	}
}

void Debug::printBuffer(const uint8_t *pBuffer, size_t count) const
{
	while(count)
	{
		printHex(*pBuffer++, 1, true);
		DebugA::archImp()->printChar(' ');
		--count;
	}
}

const Debug& Debug::operator<<(const char* aString) const
{
	const char* p = aString;
	while(*p)
	{
		DebugA::archImp()->printChar(static_cast<uint8_t>(*p));
		p++;
	}

	processPostFlags();
	return *this;
}

const Debug& Debug::operator<<(const size_t value) const
{
	print(value, false);

	processPostFlags();
	return *this;
}

const Debug&Debug::operator<<(const dhex hexValue) const
{
	printHex(hexValue.value(), hexValue.size(), false);

	processPostFlags();
	return *this;
}

void Debug::processPostFlags() const
{
	if (m_flags & DEBUG_FLAGS_SPACE_AFTER_EACH)
		DebugA::archImp()->printChar(' ');
	else
		if (m_flags & DEBUG_FLAGS_CRLF_AFTER_EACH)
			DebugA::archImp()->printChar('\n');
}

void Debug::print(size_t value, bool leadingZeros) const
{
	bool wasNumber = leadingZeros;
	size_t digit = 10;
	while (digit == static_cast<size_t>(static_cast<size_t>(digit*10)/10))
	{
		digit *= 10;
	}

	while (digit > 0)
	{
		uint8_t charValue = static_cast<uint8_t>(value / digit);

		value %= digit;
		if (charValue != 0)
		{
			wasNumber = true;
			DebugA::archImp()->printChar(charValue + '0');
		}
		else if (wasNumber || digit == 1)
		{
			DebugA::archImp()->printChar('0');
		}

		digit /= 10;
	}
}

void Debug::printHex(size_t value, size_t size, bool leadingZeros) const
{
	if ( size <= 0 )
		return;

	int count = size;
	uint8_t* pValues = &reinterpret_cast<uint8_t*>(&value)[size - 1];
	uint8_t c;

	if (m_flags & DEBUG_FLAGS_PRINT_PREFIX)
	{
		DebugA::archImp()->printChar('0');
		DebugA::archImp()->printChar('x');
	}

	bool valuePrinted = false;
	while(count--)
	{
		c = *pValues;

		// only 2 iterations in this loop
		// first iteration is to get upper byte part 0xF0 value
		// second iteration is to get lower byte part 0x0F value
		for (int i = 0; i < 2; ++i)
		{
			uint8_t charValue = (c >> (4*(1-i))) & 0x0F;

			if (c || leadingZeros)
				valuePrinted = true;

			if (c || valuePrinted)
				DebugA::archImp()->printChar(hexToASCII(charValue));
		}

		pValues--;
	}

	if (!valuePrinted)
		DebugA::archImp()->printChar('0');
}
