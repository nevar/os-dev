/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef NEW_H
#define NEW_H

//#include "Memory/SimpleAllocator.h"
//#include "Memory/PhysicalAllocator.h"
//#include "Memory/VirtualMemory.h"

extern "C++"
{
class mem {
public:
    struct nofree_t { };
    static nofree_t nofree;
	static nofree_t free;

//    static SimpleAllocator simpleAllocator;
//    static PhysicalAllocator physicalAllocator;
//    static VirtualMemory virtualMemory;
};
}

inline void *operator new(size_t, void *p) throw() { return p; }
inline void *operator new[](size_t, void *p) throw() { return p; }
inline void  operator delete  (void *, void *) throw() { }
inline void  operator delete[](void *, void *) throw() { }

void *operator new(size_t size, const mem::nofree_t& s = mem::free) throw();
void *operator new[](size_t size, const mem::nofree_t& s) throw();
void operator delete(void *, size_t size, const mem::nofree_t& ) throw();
void operator delete[](void *, size_t size, const mem::nofree_t& ) throw();

void operator delete(void *) throw();

#define HANG while(true) {}

#endif // NEW_H
