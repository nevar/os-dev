/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "stdc++.h"

extern "C"
{

// FIXME do I need to implement this or use implementation from other lib?
//void __sync_synchronize() noexcept
//{

//}

void __cxa_pure_virtual()
{
	while(1) {}
}

// FIXME implement locking http://infocenter.arm.com/help/topic/com.arm.doc.ihi0043d/IHI0043D_rtabi.pdf
int __cxa_guard_acquire(int *gv)
{
	if (*gv)
		return 0;

	return 1;
}

// FIXME implement locking
void __cxa_guard_release(int *gv)
{
	*gv = 1;
}

// There is no need to call exit functions because kernel never exits?
int atexit(void (*function)(void))
{
	return 0;
}

}
size_t strlen(const char* str)
{
	size_t s = 0;
	while(*str != 0)
	{
		++s;
		++str;
	}

	return s;
}
