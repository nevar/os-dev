/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef STDINT_H
#define STDINT_H

#include <stddef.h>
#include <stdint.h>
#include "Base/new.h"

/** @brief Returns number of bytes in "x" pages. */
#define Page(x) (0x1000 * x) // TODO replace 0x1000 with PAGE_SIZE

/** @brief Returns number of bytes in "x" kilobytes. */
#define KB(x) (0x400 * x)

/** @brief Returns number of bytes in "x" megabytes. */
#define MB(x) (0x100000 * x)

/** @brief Returns number of bytes in "x" gigabytes. */
#define GB(x) (0x40000000 * x)

#define sizeofb(x) ( sizeof(x) * 8 )

#define typeof(x) __typeof__(x)

#define Align(x,y) ( AlignCountT<typeof(x)>(x,y) * y )

#define AlignCount(x,y) AlignCountT(x,y)

template <class T> T AlignCountT(T value, T alignment)
{
	return value == 0 ? 0 : 1 + ( ( value - 1 ) / alignment );
}

size_t strlen(const char* str);

namespace std
{
template <class T> class numeric_limits
{
public:
	static T max()
	{
		if ( sizeof(T) == 1 )
		{
			return static_cast<T>(0xFF);
		}
		else if ( sizeof(T) == 2 )
		{
			return static_cast<T>(0xFFFF);
		}
		else if ( sizeof(T) == 4 )
		{
			return static_cast<T>(0xFFFFFFFF);
		}
		else if ( sizeof(T) == 8 )
		{
			return static_cast<T>(0xFFFFFFFFFFFFFFFF);
		}
	}
};
};

template <class T> T invertBytes(T t)
{
	uint8_t* v = reinterpret_cast<uint8_t*>(&t);

	for(uint8_t i = 0; i < (sizeof(T) >> 1); ++i)
	{
		uint8_t c = v[i];
		v[i] = v[sizeof(T)-i-1];
		v[sizeof(T)-i-1] = c;
	}
	return t;
}

// FIXME move this somwere
extern "C" uint8_t inb( uint16_t port );
extern "C" void outb( uint16_t port, uint8_t value );

#endif // STDINT_H
