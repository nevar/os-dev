/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

// TODO move this to operators file
void operator delete(void*) {}

/**
 * @brief Call all the static constructors in the list
 */
//extern "C" void _init()
//{
//    extern uint64_t start_ctors;
//    extern uint64_t end_ctors;

//    for(uint64_t *constructor(&start_ctors); constructor < &end_ctors; ++constructor)
//    {
//       ((void (*) (void)) (*constructor)) ();
//    }
//}

/**
 * @brief Call all the static destructors in the list
 */
//extern "C" void _fini()
//{
//    extern uint64_t start_dtors;
//    extern uint64_t end_dtors;

//    for(uint64_t *destructor(&start_dtors); destructor < &end_dtors; ++destructor)
//    {
//       ((void (*) (void)) (*destructor)) ();
//    }
//}

//class TC
//{
//public:
//	TC() : val(1) { }

//	int val;
//};

#include "Memory/PhysicalMemoryManager.h"
#include "Debug/Debug.h"

extern "C" void kernel_main()
{
	INF << "Starting" << dhex(static_cast<size_t>(0));

	PMM::printDebugInfo();

//    _init();

//    INF << "Free physical memory: " << mem::physicalAllocator.freeMemoryPagesCount() * mem::physicalAllocator.pageSize();

//    _fini();
//    kInfo() << "finished";
//	char *p = 0;
//	p[-1] = 0;

//	INF << "Finished";
	while(true) {}
}
