#!/usr/bin/env bash
# Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

set -e

DIR=$(cd $(dirname "$0"); pwd)

if [ "${OS_DIR}" = "" ]; then
    . ${DIR}/../init.env
fi

VERSION=1.5.0
DATE=2013.06
NAME=bochs-20140407
FILE=$NAME.tar.gz

if [ -d ${OS_INSTALL_DIR}/bochs ]; then
    exit 0
fi

#cd ${OS_DOWNLOADS_DIR} && wget -c http://bochs.sourceforge.net/svn-snapshot/${FILE}

if [ ! -d ${OS_BUILD_DIR}/${NAME} ]; then
    cd ${OS_BUILD_DIR} && tar -xvf ${OS_DOWNLOADS_DIR}/${FILE}
fi

cd ${OS_BUILD_DIR}/${NAME}
${OS_BUILD_DIR}/${NAME}/configure \
	--prefix=${OS_INSTALL_DIR}/bochs \
	--enable-sb16 \
	--enable-ne2000 \
	--enable-all-optimizations \
	--enable-cpu-level=6 \
	--enable-x86-64 \
	--enable-pci \
	--enable-clgd54xx \
	--enable-usb \
	--enable-usb-ohci \
	--enable-plugins \
	--enable-show-ips \
	--with-nogui \
	--enable-debugger \
	--disable-debugger-gui

make -j $CPUS

rm -rf ${OS_INSTALL_DIR}/bochs
make install
echo "Bosch installed"

