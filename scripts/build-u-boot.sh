#!/usr/bin/env bash
# Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

set -e

if [ "${1}" = "" ]; then \
        echo -e "Platform name must be set:\n\t$0 <name>"
        echo -e "avaliable names:"
        echo -e "beaglexm"
	echo -e "pandaes"
else
	# download uboot source
	if [ ! -d u-boot ]; then
		echo "Downloading u-boot source."
		wget https://launchpad.net/u-boot-linaro/trunk/13.01/+download/u-boot-linaro-2013.01.1.tar.gz
		tar -xvf u-boot-linaro-2013.01.1.tar.gz
		mv recipe-{debupstream}+{revno}+{revno:packaging}+201301240847 u-boot
	fi

	CONFIG=
	if [ "$1" = "beaglexm" ]; then
		CONFIG=omap3_beagle_config
	elif [ "$1" = "pandaes" ]; then
		CONFIG=omap4_panda_config
	fi

	rm -rf u-boot-$1
	mkdir -p u-boot-$1
	cd u-boot
	gmake CROSS_COMPILE=/usr/local/bin/arm-none-eabi- O=../u-boot-$1 ${CONFIG}
	gmake CROSS_COMPILE=/usr/local/bin/arm-none-eabi- O=../u-boot-$1
	mkdir -p ${OS_INSTALL_DIR}/platform/$1/boot
fi
