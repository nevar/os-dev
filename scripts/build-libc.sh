#!/usr/bin/env bash
# Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

set -e

DIR=$(cd $(dirname "$0"); pwd)
VERSION=0.9.14

if [ "$OS_DIR" = "" ]; then
    . ${DIR}/../init.env
fi

if [ $# -ne 2 ]; then
    echo "ERROR: Missing arguments."
    exit 1
fi

CROSS=${1}
ARCH=${2}

echo "cross: $CROSS"
echo "arch:  $ARCH"

if [ ! -d ${OS_INSTALL_DIR}/musl-${2} ]; then
    if [ ! -f ${OS_DOWNLOADS_DIR}/musl-${VERSION}.tar.gz ]; then
        cd ${OS_DOWNLOADS_DIR}
        wget http://www.musl-libc.org/releases/musl-${VERSION}.tar.gz
    fi

    rm -rf ${OS_BUILD_DIR}/musl-${VERSION}-${2}
    cd ${OS_BUILD_DIR}
    tar -xvf ${OS_DOWNLOADS_DIR}/musl-${VERSION}.tar.gz
    mv musl-${VERSION} musl-${VERSION}-${2}
    cd musl-${VERSION}-${2}
    echo ./configure --disable-shared --target=${ARCH} CROSS_COMPILE=${CROSS} --prefix=${OS_INSTALL_DIR}/musl-${2}
    ./configure --disable-shared --target=${ARCH} CC=${CROSS}gcc CROSS_COMPILE=${CROSS} --prefix=${OS_INSTALL_DIR}/musl-${2}
    make -j ${CPUS}
    rm -rf ${OS_INSTALL_DIR}/musl-${2}
    make install
fi
