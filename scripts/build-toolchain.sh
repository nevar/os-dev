#!/usr/bin/env bash
# Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

set -e

if [ "$OS_DIR" = "" ]; then
        echo "ERROR: Environment not set, set OS_DIR"
        exit 1
fi

TOOLCHAIN_DIR=${OS_CONFIG_DIR}/toolchain
CONFIG_DIR=${TOOLCHAIN_DIR}/config
REQ_OPTIONS="CT_PARALLEL_JOBS CT_LOCAL_TARBALLS_DIR CT_PREFIX_DIR"

if [ "${1}" = "" ]; then \
	echo -e "Toolchain name must be set:\n\t$0 <name>"
	echo -e "avaliable names:"
	echo `ls -1 $CONFIG_DIR`
else
	if [ "${2}" = "pack" ]; then
		if [ ! -d ${OS_INSTALL_DIR}/toolchain_${1} ]; then
			echo "ERROR: Toolchain not found on path: ${OS_INSTALL_DIR}/toolchain_${1}"
			exit 1
		fi

		cd ${OS_INSTALL_DIR}
                tar -zcvf ${TOOLCHAIN_DIR}/toolchain_${1}.tar.gz toolchain_${1}
	elif [ "${2}" = "unpack" ]; then
		if [ -d ${OS_INSTALL_DIR}/toolchain_${1} ]; then
			echo "ERROR: Toolchain already exists on: ${OS_INSTALL_DIR}/toolchain_${1}"
			exit 1
		fi
		cd ${OS_INSTALL_DIR}
		tar -xvf ${TOOLCHAIN_DIR}/toolchain_${1}.tar.gz
	else
		# cehck crosstool-ng
		if [ ! -d ${OS_INSTALL_DIR}/ct-ng ]; then
			echo "Building ct-ng"
			${OS_DIR}/scripts/build-ct-ng.sh
		fi

		if [ -d ${OS_INSTALL_DIR}/toolchain_$1 ]; then
			echo "Toolchain already installed at ${OS_INSTALL_DIR}/toolchain_$1"
			exit 0
		fi

		# check if configuration file exists
		if [ ! -f $CONFIG_DIR/$1 ]; then
			echo "ERROR: Missing configuration file: ${CONFIG_DIR}/${1}"
			exit 0
		fi

		if [ -d ${OS_BUILD_DIR}/toolchain_$1 ]; then
			rm -rf $OS_BUILD_DIR/toolchain_$1
		fi

		mkdir -p ${OS_BUILD_DIR}/toolchain_$1
		cd ${OS_BUILD_DIR}/toolchain_$1
		cp ${CONFIG_DIR}/${1} ./.config_main

	        # set options
	        for OPTION in ${REQ_OPTIONS}; do
	                if [ ! `cat .config_main | grep -c ${OPTION}` -gt 0 ]; then
	                        echo -e "ERROR: File ${CONFIG_DIR}/$1 does not contain \"$OPTION\" option"
	                        exit 0
	                fi
	        done

	        cat .config_main \
	        | sed "s:CT_PARALLEL_JOBS=\([0-9]\)*:CT_PARALLEL_JOBS=${CPUS}:" \
	        | sed "s:CT_LOCAL_TARBALLS_DIR=.*:CT_LOCAL_TARBALLS_DIR=\"${OS_DOWNLOADS_DIR}\":" \
	        | sed "s:CT_PREFIX_DIR=.*:CT_PREFIX_DIR=\"${OS_DIR}/tmp/toolchain_${1}\":" \
	        > .config

		if [ -d ${OS_DIR}/tmp/toolchain_${1} ]; then
			rm -rf ${OS_DIR}/tmp/toolchain_${1}
		fi

		${OS_INSTALL_DIR}/ct-ng/bin/ct-ng build

#                if [ -d ${OS_BUILD_DIR}/toolchain_$1 ]; then
#                        rm -rf ${OS_BUILD_DIR}/toolchain_$1
#                fi

		chmod -R +w ${OS_DIR}/tmp/toolchain_${1}
		mv ${OS_DIR}/tmp/toolchain_${1} ${OS_INSTALL_DIR}/		

		echo Toolchain ${1} done
	fi
fi


