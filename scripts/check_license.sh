#!/usr/bin/env bash
# Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# ./scripts/check_license.sh license.txt

DIR=/tmp/clic
CPP_FILES=$(find -E . -regex '(.*\.cpp$|.*\.h$)')
SCRIPT_FILES=$(find -E . -regex '(.*\.sh$)')

rm -rf $DIR
mkdir -p $DIR

# generate license file
echo '/*' > $DIR/license_cpp.txt
sed -e 's/^/ \*/g' license.txt >> $DIR/license_cpp.txt
echo ' */' >> $DIR/license_cpp.txt
LICENSE_CPP=$DIR/license_cpp.txt

# generate license file
sed -e 's/^/#/g' license.txt >> $DIR/license_sh.txt
LICENSE_SH=$DIR/license_sh.txt

LINES=$(cat $LICENSE_CPP | wc -l)
for f in $CPP_FILES
do
	F=$(basename $f)
	head -n $LINES $f > $DIR/$F
	diff -q -r -0 $DIR/$F $LICENSE_CPP
done

LINES=$(cat $LICENSE_SH | wc -l)
for f in $SCRIPT_FILES
do
	F=$(basename $f)
	head -n $(($LINES+1)) $f | tail -n $LINES > $DIR/$F
	diff -q -r -0 $DIR/$F $LICENSE_SH
done
