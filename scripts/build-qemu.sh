#!/usr/bin/env bash
# Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

set -e

VERSION=1.7.0
DATE=2014.01
NAME=qemu-linaro-$VERSION-$DATE
FILE=$NAME.tar.gz
CPUS=8
INSTALL_DIR=/tmp/qemu

if [ ! -f $FILE ]
then
	wget https://launchpad.net/qemu-linaro/trunk/$DATE/+download/$FILE
fi

if [ ! -d ${NAME} ]; then
	tar -xvf $FILE
	cd ${NAME}
	git clone https://github.com/qemu/dtc.git
	./configure --prefix=${INSTALL_DIR} --target-list=arm-softmmu --enable-sdl --disable-werror
else
	cd ${NAME}
fi

if [ ! -d ${INSTALL_DIR} ]; then
	gmake -j $CPUS
	gmake install
	rm -rf seabios
fi

# Developers from qemu-linaro removed all bios images due to some license issues.
# Now we need to get bios and vga bios.

if [ ! -d seabios ]; then
	git clone git://git.seabios.org/seabios.git
	cd seabios
	gmake
	cp out/bios.bin ${INSTALL_DIR}/share/qemu/
fi

if [ ! -f ${INSTALL_DIR}/share/qemu/vgabios-cirrus.bin ]; then
	cd ${INSTALL_DIR}/share/qemu
	wget http://savannah.gnu.org/cgi-bin/viewcvs/*checkout*/vgabios/vgabios/VGABIOS-lgpl-latest.cirrus.bin
	mv ./VGABIOS-lgpl-latest.cirrus.bin ./vgabios-cirrus.bin
fi
