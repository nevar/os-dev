#!/usr/bin/env bash
# Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

if [ "$OS_DIR" = "" ]; then
        echo "ERROR: Environment not set, set OS_DIR"
        exit 1
fi

if [ ! -f ${OS_DIR}/os/image/sd.img ];
then
	echo "Generating ${OS_DIR}/os/image/sd/sd.img"
	mkdir -p ${OS_DIR}/os/image/sd
	mkdiskimage -F -M ${OS_DIR}/os/image/sd/sd.img 512 255 63
fi
